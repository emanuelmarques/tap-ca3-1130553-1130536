package isep.tap.ca3.crosscutting.log

trait Log{
  def log(message: String): Unit
  
  def top(): (String, String)
}