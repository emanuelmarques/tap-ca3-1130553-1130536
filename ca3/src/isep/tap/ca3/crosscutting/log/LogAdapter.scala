package isep.tap.ca3.crosscutting.log

import java.util.Calendar
import isep.tap.ca3.logger.TimeTagLogger

package object LogAdapter {
  implicit class LogAdapter(ttl: TimeTagLogger) extends Log{
    def log(message: String): Unit = {
      val timestamp = Calendar.getInstance().getTime().toString()
      ttl.log(message, timestamp)
    }
    
    def top() = ttl.top()
  } 
}