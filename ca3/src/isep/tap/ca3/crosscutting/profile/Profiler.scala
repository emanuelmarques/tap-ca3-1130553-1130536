package isep.tap.ca3.crosscutting.profile

import isep.tap.ca3.dataReader.DataReader

trait Profiler extends DataReader {
  
  abstract override def readDataFast(filePath: String): ProfilerList[Float] =
    {
      //Time before reading the Data
      val startTime = System.currentTimeMillis()

      val recipes = super.readDataFast(filePath)

      //Time after reading the Data
      val endTime = System.currentTimeMillis()

      //Returns the list and the elapsed time
      new ProfilerList(recipes.lr, endTime - startTime)
    }

  abstract override def readDataSlow(filePath: String): ProfilerList[Float] =
    {
      //Time before reading the Data
      val startTime = System.currentTimeMillis()

      val recipes = super.readDataSlow(filePath)

      //Time after reading the Data
      val endTime = System.currentTimeMillis()

      //Returns the list and the elapsed time
      new ProfilerList(recipes.lr, endTime - startTime)
    }
}

