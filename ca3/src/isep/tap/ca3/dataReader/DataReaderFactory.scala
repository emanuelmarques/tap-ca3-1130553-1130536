package isep.tap.ca3.dataReader

object DataReaderFactory {

  private class DataReaderImpl extends DataReader{
    def read(filename: String): RecipeList = {
      val strategy = DataReaderFactory.apply(filename)
      strategy match{
        case None => throw new Exception("Strategy not found")
        case _ => strategy.get(filename)
      }
    }
  }

  def newDataReader() : DataReader = new DataReaderImpl
  
  private def apply(filename: String): Option[(String) => RecipeList] =
    filename match {
      case f if f.endsWith(".json") => Some(parseJson)
      case f if f.endsWith(".csv")  => Some(parseCsv)
      case f                        => None
    }
  
  private def parseJson(file: String): RecipeList = {
    val reader: DataReader = new DataReaderImplJSON
    reader.read(file)
  }
  
  private def parseCsv(file: String): RecipeList = {
    val reader: DataReader = new DataReaderImplCSV
    reader.read(file)
  }

}