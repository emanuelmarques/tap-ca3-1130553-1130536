package isep.tap.ca3.dataReader

import com.github.tototoshi.csv._
import java.io.File
import isep.tap.ca3.model.Recipe

class DataReaderImplCSV extends DataReader {

  override def read(fname: String): RecipeList = {
    val pwd: String = System.getProperty("user.dir") + "/files/" + fname
    val reader = CSVReader.open(new File(pwd))
    val lines = reader.all() 
    reader.close()
    
    val recipeList = lines.drop(1).map(l => Recipe(l.head, l.last)) //drop(1) to ignore headers
    
    new RecipeList(recipeList)
  }

}
