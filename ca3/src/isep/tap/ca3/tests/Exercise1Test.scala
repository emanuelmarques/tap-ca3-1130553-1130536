package isep.tap.ca3.tests

import org.scalatest.FunSuite
import isep.tap.ca3.model.Recipe
import isep.tap.ca3.dataReader.DataReaderImplJSON

import isep.tap.ca3.crosscutting.profile.Profiler


class Exercise1Test extends FunSuite {

  test("Exercise 1...") {
 
    val dataReader = new DataReaderImplJSON with Profiler
    
    val expected = List(Recipe("Portuguese Green Soup","Portugal"), Recipe("Grilled Sardines","Portugal"), Recipe("Salted Cod with Cream","Portugal"))
    
    val l1 = dataReader.readDataFast("recipe.json")
    val l2 = dataReader.readDataSlow("recipe.json")

    assert(l1.lr===expected)
    assert(l2.lr===expected)
    assert(l1.pt<l2.pt)

  }
}