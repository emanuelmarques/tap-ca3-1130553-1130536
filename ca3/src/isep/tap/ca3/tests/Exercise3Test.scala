package isep.tap.ca3.tests

import org.scalatest.FunSuite
import isep.tap.ca3.model.Recipe
import isep.tap.ca3.dataReader.DataReaderImplCSV
import isep.tap.ca3.dataReader.DataReader
import isep.tap.ca3.dataReader.DataReaderFactory

class Exercise3Test extends FunSuite {
  
  test("Exercise 3 initial conditions...") {
    val dataReader = new DataReaderImplCSV
    
    val expected = List(Recipe("Portuguese Green Soup","Portugal"), Recipe("Grilled Sardines","Portugal"), Recipe("Salted Cod with Cream","Portugal"))
    val other_expected =List(Recipe("German Sauerkraut","Germany"),Recipe("Risotto","Italy"),Recipe("Tortilla","Spain"))

    assert(dataReader.readDataFast("recipe.csv").lr===expected)
    assert(dataReader.readDataSlow("other_recipe.csv").lr===other_expected)
  }

  // strategy does not exist
  test("Exercise 3. Test 1.") {
    val reader: DataReader = DataReaderFactory.newDataReader()
    try{
      val recipes = reader.read("recipe.txt")
      fail()
    }catch{
      case e: Exception => assert(e.getMessage==="Strategy not found")  
    }
  }
  
  // read one file JSON and another csv, with same content, verify equality
  test("Exercise 3. Test 2.") {    
    val reader: DataReader = DataReaderFactory.newDataReader()
    val jsonFileName = "recipe.json"
    val csvFileName = "recipe.csv"
    val jsonResult = reader.read(jsonFileName)
    val csvResult = reader.read(csvFileName)
    
    assert(jsonResult.lr===csvResult.lr)    
  }

  // read one file JSON and another csv, with different content, verify inequality
  test("Exercise 3. Test 3.") { 
     val reader: DataReader = DataReaderFactory.newDataReader()
    val jsonFileName = "recipe.json"
    val csvFileName = "other_recipe.csv"
    val jsonResult = reader.read(jsonFileName)
    val csvResult = reader.read(csvFileName)
    
    assert(jsonResult.lr!==csvResult.lr)    
  }
}